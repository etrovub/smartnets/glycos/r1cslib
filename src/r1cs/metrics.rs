use bulletproofs::r1cs::Metrics as BPMetrics;

#[derive(Debug, Clone)]
pub struct Metrics {
    pub multipliers: usize,
    pub constraints: usize,
    pub phase_one_constraints: usize,
    pub phase_two_constraints: usize,
}

impl From<BPMetrics> for Metrics {
    fn from(m: BPMetrics) -> Self {
        Self {
            multipliers: m.multipliers,
            constraints: m.constraints,
            phase_one_constraints: m.constraints,
            phase_two_constraints: m.constraints,
        }
    }
}
