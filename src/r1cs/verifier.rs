#![allow(non_snake_case)]

use core::borrow::BorrowMut;
use curve25519_dalek::scalar::Scalar;
use merlin::Transcript;

use super::{
    ConstraintSystem, LinearCombination, R1CSProof, Variable,
};

use crate::errors::R1CSError;
use bulletproofs::{BulletproofGens, PedersenGens};
use bulletproofs::r1cs::Verifier as DalekBpVerifier;
use bulletproofs::r1cs::ConstraintSystem as BPConstraintSystem;
use crate::r1cs::{Metrics, proof_system::DalekBulletproofs};

pub trait VerifierTrait {}
impl<T: BorrowMut<Transcript>> VerifierTrait for DalekBpVerifier<T>{}

pub struct Verifier<T: VerifierTrait> {
    verifier: T,
}

impl<T: BorrowMut<Transcript>> ConstraintSystem for Verifier<DalekBpVerifier<T>> {
    fn transcript(&mut self) -> &mut Transcript {
        self.verifier.transcript()
    }

    fn multiply(
        &mut self,
        left: LinearCombination,
        right: LinearCombination,
    ) -> (Variable, Variable, Variable) {
        let (var1, var2, var3) = self.verifier.multiply(left.into(), right.into());
        (var1.into(), var2.into(), var3.into())
    }

    fn allocate(&mut self, _foo: Option<Scalar>) -> Result<Variable, R1CSError> {
        self.verifier.allocate(_foo).map_or_else(|err| Err(err.into()), |var| Ok(var.into()))
    }

    fn allocate_multiplier(
        &mut self,
        _foo: Option<(Scalar, Scalar)>,
    ) -> Result<(Variable, Variable, Variable), R1CSError> {
        self.verifier.allocate_multiplier(_foo)
            .map_or_else(|err| Err(err.into()),
                         |(var1, var2, var3)| Ok((var1.into(), var2.into(), var3.into())))
    }

    fn metrics(&self) -> Metrics {
        self.verifier.metrics().into()
    }

    fn constrain(&mut self, lc: LinearCombination) {
        self.verifier.constrain(lc.into())
    }
}

impl<T: BorrowMut<Transcript>> Verifier<DalekBpVerifier<T>> {
    pub fn new(transcript: T) -> Self {
        Self{verifier: DalekBpVerifier::new(transcript)}
    }

    pub fn verify(
        self,
        proof: &R1CSProof<DalekBulletproofs>,
        pc_gens: &PedersenGens,
        bp_gens: &BulletproofGens,
    ) -> Result<(), R1CSError> {
        self.verifier.verify(&proof.proof, pc_gens, bp_gens).map_err(|err| err.into())
    }

    pub fn verify_and_return_transcript(
        self,
        proof: &R1CSProof<DalekBulletproofs>,
        pc_gens: &PedersenGens,
        bp_gens: &BulletproofGens,
    ) -> Result<T, R1CSError> {
        self.verifier.verify_and_return_transcript(&proof.proof, pc_gens, bp_gens).map_err(|err| err.into())
    }
}
