use bulletproofs::r1cs::R1CSProof as BPR1CSProof;

pub trait ProofSystem{
    type Proof: Sized;
}

pub struct DalekBulletproofs{}
impl ProofSystem for DalekBulletproofs{
    type Proof = BPR1CSProof;
}
