#![allow(non_snake_case)]

use core::borrow::BorrowMut;
use curve25519_dalek::ristretto::CompressedRistretto;
use curve25519_dalek::scalar::Scalar;
use merlin::Transcript;
use super::{
    ConstraintSystem, LinearCombination, R1CSProof, Variable,
};
use crate::errors::R1CSError;
use crate::r1cs::{Metrics, proof_system::DalekBulletproofs};
use bulletproofs::r1cs::Prover as DalekBpProver;
use bulletproofs::r1cs::ConstraintSystem as BPConstraintSystem;
use bulletproofs::{BulletproofGens, PedersenGens};

pub trait ProverTrait {}
impl<'g, T: BorrowMut<Transcript>> ProverTrait for DalekBpProver<'g, T>{}

pub struct Prover<T: ProverTrait> {
    prover: T,
}

impl<'g, T: BorrowMut<Transcript>> ConstraintSystem for Prover<DalekBpProver<'g, T>> {
    fn transcript(&mut self) -> &mut Transcript {
        self.prover.transcript()
    }

    fn multiply(
        &mut self,
        left: LinearCombination,
        right: LinearCombination,
    ) -> (Variable, Variable, Variable) {
        let (v1, v2, v3) = self.prover.multiply(left.into(), right.into());
        (v1.into(), v2.into(), v3.into())
    }

    fn allocate(&mut self, assignment: Option<Scalar>) -> Result<Variable, R1CSError> {
        self.prover.allocate(assignment).map_or_else(|err| Err(err.into()), |var| Ok(var.into()))
    }

    fn allocate_multiplier(
        &mut self,
        input_assignments: Option<(Scalar, Scalar)>,
    ) -> Result<(Variable, Variable, Variable), R1CSError> {
        self.prover.allocate_multiplier(input_assignments).map_or_else(
            |err| Err(err.into()),
            |(var1, var2, var3)| Ok((var1.into(), var2.into(), var3.into()))
            )
    }

    fn metrics(&self) -> Metrics {
        self.prover.metrics().into()
    }

    fn constrain(&mut self, lc: LinearCombination) {
        self.prover.constrain(lc.into())
    }
}

impl<'g, T: BorrowMut<Transcript>> Prover<DalekBpProver<'g, T>> {
    pub fn new(pc_gens: &'g PedersenGens, transcript: T) -> Self {
        Self{prover: DalekBpProver::new(pc_gens, transcript)}
    }

    pub fn commit(&mut self, v: Scalar, v_blinding: Scalar) -> (CompressedRistretto, Variable) {
        let (cr, var) = self.prover.commit(v, v_blinding);
        (cr, var.into())
    }

    /// Returns the secret value of the linear combination.
    pub fn eval(&self, lc: &LinearCombination) -> Scalar {
        self.prover.eval(&lc.clone().into()).into()
    }

    /// Consume this `ConstraintSystem` to produce a proof.
    pub fn prove(self, bp_gens: &BulletproofGens) -> Result<R1CSProof<DalekBulletproofs>, R1CSError> {
        self.prover.prove(bp_gens).map_or_else(|error| Err(error.into()), |proof| Ok(R1CSProof{proof}))
    }

    /// Consume this `ConstraintSystem` to produce a proof. Returns the proof and the transcript passed in `Prover::new`.
    pub fn prove_and_return_transcript(
        self,
        bp_gens: &BulletproofGens,
    ) -> Result<(R1CSProof<DalekBulletproofs>, T), R1CSError> {
        self.prover.prove_and_return_transcript(bp_gens).map_or_else(|err| Err(err.into()), |(proof, t)| Ok((R1CSProof{proof}, t)))
    }
}
