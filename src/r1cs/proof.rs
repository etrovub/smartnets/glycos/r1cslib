#![allow(non_snake_case)]
//! Definition of the proof struct.

use crate::errors::R1CSError;

use crate::r1cs::proof_system::{ProofSystem, DalekBulletproofs};

use serde::de::Visitor;
use serde::{self, Deserialize, Deserializer, Serialize, Serializer};

#[derive(Clone, Debug)]
#[allow(non_snake_case)]
pub struct R1CSProof<T: ProofSystem>{
    pub proof: T::Proof,
}

impl R1CSProof<DalekBulletproofs> {
    pub fn to_bytes(&self) -> Vec<u8> {
        self.proof.to_bytes()
    }

    /// Returns the size in bytes required to serialize the `R1CSProof`.
    pub fn serialized_size(&self) -> usize {
        self.proof.serialized_size()
    }

    /// Deserializes the proof from a byte slice.
    ///
    /// Returns an error if the byte slice cannot be parsed into a `R1CSProof`.
    pub fn from_bytes(slice: &[u8]) -> Result<Self, R1CSError> {
        <DalekBulletproofs as ProofSystem>::Proof::from_bytes(slice).map_or_else(|error| Err(error.into()), |proof| Ok(Self{proof}))
        }
}

impl Serialize for R1CSProof<DalekBulletproofs> {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        serializer.serialize_bytes(&self.to_bytes()[..])
    }
}

impl<'de> Deserialize<'de> for R1CSProof<DalekBulletproofs> {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        struct R1CSProofVisitor;

        impl<'de> Visitor<'de> for R1CSProofVisitor {
            type Value = R1CSProof<DalekBulletproofs>;

            fn expecting(&self, formatter: &mut ::core::fmt::Formatter) -> ::core::fmt::Result {
                formatter.write_str("a valid R1CSProof")
            }

            fn visit_bytes<E>(self, v: &[u8]) -> Result<R1CSProof<DalekBulletproofs>, E>
            where
                E: serde::de::Error,
            {
                // Using Error::custom requires T: Display, which our error
                // type only implements when it implements std::error::Error.
                #[cfg(feature = "std")]
                return R1CSProof::from_bytes(v).map_err(serde::de::Error::custom);
                // In no-std contexts, drop the error message.
                #[cfg(not(feature = "std"))]
                return R1CSProof::from_bytes(v)
                    .map_err(|_| serde::de::Error::custom("deserialization error"));
            }
        }
        <DalekBulletproofs as ProofSystem>::Proof::deserialize(deserializer).map(|proof| Self {proof})
    }
}
