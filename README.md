Abstractions for R1CS based on [dalek cryptography's bulletproofs](https://github.com/dalek-cryptography/bulletproofs).


Supported Proof Systems:

 - Dalek Bulletproofs

 Planned Proof Systems:

 - Spartan

*Note: Range proofs are currently not supported.*
